#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

int main(int argc,char **argv) {
  MPI_Comm comm;
  int ntids,mytid;
  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm,&ntids);
  MPI_Comm_rank(comm,&mytid);

  {
    MPI_Status status;
    int my_number = mytid, other_number=-1.;
    int successor, previous;
    MPI_Request reqs[2];
    MPI_Status stats[2];
    MPI_Info info;
    MPI_Win win;
    previous=mytid-1;
    if (mytid<ntids-1){
      successor=mytid+1;
    }
    else{
      successor=MPI_PROC_NULL;
    }
    if (mytid<=0){
      previous=MPI_PROC_NULL;
    }
    MPI_Info_create(&info);
    MPI_Win_create(&my_number,1,sizeof(int),info,comm,&win);
    MPI_Get( /*data: */ &my_number,1,MPI_INT,
             /*to:   */ successor,0,1,MPI_INT,win);
    MPI_Win_fence(MPI_MODE_NOSUCCEED,win);
    MPI_Win_free(&win);
    //  MPI_Isend( /*data: */ &my_number,1,MPI_INT,
    //             /*to:   */ successor,0,comm, &reqs[0]);
    //  MPI_Irecv( /*data: */ &other_number,1,MPI_INT,
    //             /*from: */ previous,0,comm,&reqs[1]);
    //  MPI_Waitall(2,reqs,stats);
   // MPI_Send( /* data: */ &my_number,1,MPI_INT,
   //              /* to: */ successor, /* tag: */ 0, comm);
   // if (mytid>0)
   //   MPI_Recv( /* data: */ &other_number,1,MPI_INT,
   //             /* from: */ mytid-1, 0, comm, &status);
   // MPI_Sendrecv(/*data: */ &my_number,1,MPI_INT,
   //             /*to:   */ successor, /*tag: */ 0,
   //             /*data: */ &other_number,1,MPI_INT,
   //             /*from: */ previous, 0, comm, &status);

    /* Correctness check */
    int *gather_buffer=NULL;
    if (mytid==0) {
      gather_buffer = (int*) malloc(ntids*sizeof(int));
      if (!gather_buffer) MPI_Abort(comm,1);
    }
    MPI_Gather(&other_number,1,MPI_INT,
               gather_buffer,1,MPI_INT, 0,comm);
    if (mytid==0) {
      int i,error=0;
      for (i=0; i<ntids; i++) 
        if (gather_buffer[i]!=i-1) {
          printf("Processor %d was incorrect: %d should be %d\n",
                 i,gather_buffer[i],i-1);
          error =1;
        }
      if (!error) printf("Success!\n");
      free(gather_buffer);
    }
  }

  MPI_Finalize();
  return 0;
}
