#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mandel.h"
#include "Image.h"

//Exercise 1.19 -- Non-blocking
class bulkqueue : public queue {
public :
  bulkqueue(MPI_Comm queue_comm, circle *workcircle)
    : queue(queue_comm,workcircle) {};

  void addtask(struct coordinate xy,int proc,MPI_Request reqs[]) {
    int contribution; MPI_Status temp;
    //non blocking
    MPI_Isend(&xy,2,MPI_DOUBLE,
              proc,0,comm,&reqs[2*proc]);
    MPI_Irecv(&contribution,1,MPI_INT,
              proc,0,comm,&reqs[2*proc+1]);
    if (workcircle->is_valid_coordinate(xy)) {
      coordinate_to_image(xy,contribution);
      total_tasks++;
    }
  };
//changed complete to take in request buffer and status buffer
  void complete(MPI_Request reqs[],MPI_Status stats[]) {
    struct coordinate xy;
    workcircle->invalid_coordinate(xy);
    for (int p=0; p<ntids-1; p++){
      addtask(xy,p,reqs);
    }
//wait for all send/recvs
    MPI_Waitall(2*(ntids-1),reqs,stats);
    t_stop = MPI_Wtime();
    printf("Tasks %d in time %d\n",total_tasks,t_stop-t_start);
    image->Write();
    return;
  };
};

class serialqueue : public queue {
private :
  int free_processor;
public :
  serialqueue(MPI_Comm queue_comm,circle *workcircle)
    : queue(queue_comm,workcircle) {
    free_processor=0;
  };
  /* Send a coordinate to a free processor;
     if the coordinate is invalid, this should stop the process;
     otherwise add the result to the image.
  */
  void addtask(struct coordinate xy) {
    MPI_Status status; int contribution;

    MPI_Send(&xy,2,MPI_DOUBLE, 
	     free_processor,0,comm);
    MPI_Recv(&contribution,1,MPI_INT,
	     free_processor,0,comm, &status);
    if (workcircle->is_valid_coordinate(xy)) {
      coordinate_to_image(xy,contribution);
      total_tasks++;
    }
    free_processor++;
    if (free_processor==ntids-1)
      // wrap around to the first again
      free_processor = 0;
  };
  void complete() { 
    struct coordinate xy;
    workcircle->invalid_coordinate(xy); free_processor=0;
    for (int p=0; p<ntids-1; p++)
      addtask(xy);
    t_stop = MPI_Wtime();
    printf("Tasks %d in time %d\n",total_tasks,t_stop-t_start);
    image->Write();
    return; 
  };
};

int main(int argc,char **argv) {
  MPI_Comm comm;
  int ntids,mytid, steps,iters,ierr;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm,&ntids);
  MPI_Comm_rank(comm,&mytid);

// for bulk queue (non-blocking)
  MPI_Status *stats = (MPI_Status*) malloc( 2*(ntids-1)*sizeof(MPI_Status) );
  MPI_Request *reqs = (MPI_Request*) malloc( 2*(ntids-1)*sizeof(MPI_Request) );
//    MPI_Status stats[46];
//    MPI_Request reqs[46];

  ierr = parameters_from_commandline
    (argc,argv,comm,&steps,&iters);
  if (ierr) return MPI_Abort(comm,1);

  if (ntids==1) {
    printf("Sorry, you need at least two processors\n");
    return 1;
  }

  circle *workcircle = new circle(2./steps,iters);
  //serialqueue *taskqueue = new serialqueue(comm,workcircle);
  bulkqueue *taskqueue = new bulkqueue(comm,workcircle);
  if (mytid==ntids-1)  {
    taskqueue->set_image( new Image(2*steps,2*steps,"mandelpicture") );
    for (;;) {
      struct coordinate xy;
      int proc_tot=ntids-1;
//send all of the procs to work
      for(int i=0; i<ntids-1; i++){
        workcircle->next_coordinate(xy);
        if (workcircle->is_valid_coordinate(xy)){
	  taskqueue->addtask(xy,i,reqs);
          proc_tot--;  //this value keeps track of how many procs are free.
        }
        else{ //if invalid, leave
          break;
        }
      } 
//wait for send/recvs to finish - don't worry about procs not gotten to yet
      MPI_Waitall(2*(ntids-1-(proc_tot)),reqs,stats);      
      if (!(workcircle->is_valid_coordinate(xy)))
        break;
    }
    taskqueue->complete(reqs,stats);
  } else
    taskqueue->wait_for_work(comm,workcircle);

  MPI_Finalize();
  return 0;
}
