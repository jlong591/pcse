#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mandel.h"
#include "Image.h"

//Exercise 1.20 -- Non-blocking with Scatter/gather
class bulkqueue : public queue {
public :
  bulkqueue(MPI_Comm queue_comm, circle *workcircle)
    : queue(queue_comm,workcircle) {};

  void addtask(struct coordinate points[],int procs) {
    int *contribution = (int*) malloc((ntids-1)*sizeof(int));
    struct coordinate xy;
    int res;
    if(procs > 0){
//new shiny collectives
    MPI_Scatter(points,2,MPI_DOUBLE, &xy, 2,MPI_DOUBLE,ntids-1,comm);
    MPI_Gather(&res,1,MPI_INT,contribution,1,MPI_INT,ntids-1,comm);
    }
    for(int i=0;i<ntids-1;i++){
      if(workcircle->is_valid_coordinate(points[i])){
      coordinate_to_image(points[i],contribution[i]);
      total_tasks++;
      }
    }
  };
//changed complete to take in arrays
  void complete(struct coordinate points[],int ntids) {
    struct coordinate xy;
    workcircle->invalid_coordinate(xy);
    for (int p=0; p<ntids-1; p++){
      points[p]=xy;
    }
    addtask(points,ntids);
    
//wait for all send/recvs
//    MPI_Waitall(2*(ntids-1),reqs,stats);
    t_stop = MPI_Wtime();
    printf("Tasks %d in time %d\n",total_tasks,t_stop-t_start);
    image->Write();
    return;
  };
};

class serialqueue : public queue {
private :
  int free_processor;
public :
  serialqueue(MPI_Comm queue_comm,circle *workcircle)
    : queue(queue_comm,workcircle) {
    free_processor=0;
  };
  /* Send a coordinate to a free processor;
     if the coordinate is invalid, this should stop the process;
     otherwise add the result to the image.
  */
  void addtask(struct coordinate xy) {
    MPI_Status status; int contribution;

    MPI_Send(&xy,2,MPI_DOUBLE, 
	     free_processor,0,comm);
    MPI_Recv(&contribution,1,MPI_INT,
	     free_processor,0,comm, &status);
    if (workcircle->is_valid_coordinate(xy)) {
      coordinate_to_image(xy,contribution);
      total_tasks++;
    }
    free_processor++;
    if (free_processor==ntids-1)
      // wrap around to the first again
      free_processor = 0;
  };
  void complete() { 
    struct coordinate xy;
    workcircle->invalid_coordinate(xy); free_processor=0;
    for (int p=0; p<ntids-1; p++)
      addtask(xy);
    t_stop = MPI_Wtime();
    printf("Tasks %d in time %d\n",total_tasks,t_stop-t_start);
    image->Write();
    return; 
  };
};

int main(int argc,char **argv) {
  MPI_Comm comm;
  int ntids,mytid, steps,iters,ierr;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm,&ntids);
  MPI_Comm_rank(comm,&mytid);

// for bulk queue (non-blocking)
  MPI_Status *stats = (MPI_Status*) malloc( 2*(ntids-1)*sizeof(MPI_Status) );
  MPI_Request *reqs = (MPI_Request*) malloc( 2*(ntids-1)*sizeof(MPI_Request) );
//    MPI_Status stats[46];
//    MPI_Request reqs[46];
//array of points for gather/scatter
  struct coordinate *points = (struct coordinate*)malloc((ntids-1)*sizeof(struct coordinate));


  ierr = parameters_from_commandline
    (argc,argv,comm,&steps,&iters);
  if (ierr) return MPI_Abort(comm,1);

  if (ntids==1) {
    printf("Sorry, you need at least two processors\n");
    return 1;
  }

  circle *workcircle = new circle(2./steps,iters);
  //serialqueue *taskqueue = new serialqueue(comm,workcircle);
  bulkqueue *taskqueue = new bulkqueue(comm,workcircle);
  if (mytid==ntids-1)  {
    taskqueue->set_image( new Image(2*steps,2*steps,"mandelpicture") );
    for (;;) {
      struct coordinate xy;
      struct coordinate prev_xy;
      int proc_tot=ntids-1;
      int flag=1;
      for(int i=0; i<ntids-1; i++){
        workcircle->next_coordinate(xy);
        if (workcircle->is_valid_coordinate(xy)){
//keep array of coords for the scatter and gather calls
          points[i]=xy;
          proc_tot--;
        }
        else{ //if the others were invlaid, leave.
          break;
        }
      }
      taskqueue->addtask(points,ntids-proc_tot); 
      if (!(workcircle->is_valid_coordinate(xy)))
        break;
    }
    taskqueue->complete(points,ntids);
  } else{
      taskqueue->wait_for_work(comm,workcircle);
    }
  MPI_Finalize();
  return 0;
}
