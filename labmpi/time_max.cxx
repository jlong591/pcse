#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <math.h>
using namespace std;
#include <mpi.h>

int main(int argc,char **argv) {
  int mytid, numprocs;
  MPI_Comm comm;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm,&mytid);
  //got number of processors
  MPI_Comm_size(comm,&numprocs);


  {
    double tstart,tstop,jitter,avgjitt,stdvjitt_isum,stdvjitt;
    double *recbuf;
    void *sendbuf;
    int wait;

    // wait for a random time, and measure how long
    wait = (int) ( 6.*rand() / (double)RAND_MAX );
    tstart = MPI_Wtime();
    sleep(wait);
    tstop = MPI_Wtime();
    jitter = tstop-tstart-wait;

    // find the maximum time over all processors
    if (mytid==0)
      sendbuf = MPI_IN_PLACE;
    else sendbuf = (void*)&jitter;
    //Exercise 1.9
    //changed MPI_MAX to MPI_SUM (to sum instead of find max)
    MPI_Reduce((void*)&jitter,(void*)&avgjitt,1,MPI_DOUBLE,MPI_SUM,0,comm);
    if (mytid==0){
    //added line to find avg
      avgjitt = avgjitt/numprocs;
    //Exercise 1.10

      printf("average OS jitter: %e\n",avgjitt);
    }
    //Added Bcast to send average to all procs
    MPI_Bcast((void*)&avgjitt,1,MPI_DOUBLE,0,comm);
    //each thread compute internal sum of stdev
    stdvjitt_isum=pow(jitter-avgjitt,2);
    //reduce and sum up internal sums
    MPI_Reduce((void*)&stdvjitt_isum,(void*)&stdvjitt,1,MPI_DOUBLE,MPI_SUM,0,comm);
    if (mytid==0){
    //root will compute sqrt of sum and output answer
      stdvjitt = sqrt(stdvjitt/numprocs);
      printf("stdv OS jitter: %e\n",stdvjitt);
    }
    //Exercise 1.11
    //Added MPI_Gather to grab all the jitter values
    if (mytid==0){
    //allocate recbuf size
      recbuf = (double *)malloc(numprocs*sizeof(double));
    }
    MPI_Gather((void*)&jitter,1,MPI_DOUBLE,recbuf,1,MPI_DOUBLE,0,comm);
    if (mytid==0){
      printf("P%d: Gathered Jitters:\n",mytid);
      for(int i=0; i<numprocs; i++){
         printf("%e\n",recbuf[i]);
      }
      free(recbuf);
    }
  }
  MPI_Finalize();
  return 0;
}
